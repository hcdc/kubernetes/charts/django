
{{- define "django.builtinResources" -}}
limits:
  cpu: '1'
  memory: 1000Mi
requests:
  cpu: 200m
  memory: 200Mi
{{- end }}
