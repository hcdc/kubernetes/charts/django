
{{- define "django.affinity" -}}
{{- /* due to scoping we need to use a dict for requiredPodAffinity and override the value if necessary */}}
{{- $requiredPodAffinity := dict "isRequired" false }}
{{- $builtinVolumes := include "django.builtinVolumes" . | fromYaml }}
{{- $allVolumes := mustMergeOverwrite (dict) $builtinVolumes .Values.volumes }}
{{- range $name, $options := $allVolumes }}
  {{- /* Check the necessity of podAffinity */}}
  {{- if and (not $options.projected) (hasKey $options "accessModes") }}
    {{- range $options.accessModes }}
      {{- if eq . "ReadWriteOnce" }}
        {{- $_ := set $requiredPodAffinity "isRequired" true }}
      {{- end }}
    {{- end }}
  {{- end }}
{{- end }}

{{- /*
  check if we need to access volumes from nginx and django
*/}}
{{- $builtinNginxVolumes := include "nginx.builtinVolumes" . | fromYaml }}
{{- $allNginxVolumes := mustMergeOverwrite (dict) $builtinNginxVolumes .Values.nginx.volumes }}

{{- $requiredNginxPodAffinity := dict "isRequired" false }}
{{- range $name, $options := $allVolumes }}
  {{- range $nginxName, $nginxOptions := $allNginxVolumes }}
    {{- if eq $name $nginxName }}
      {{- /* Check the necessity of podAffinity */}}
      {{- if and (not $options.projected) (hasKey $options "accessModes") }}
        {{- range $options.accessModes }}
          {{- if eq . "ReadWriteOnce" }}
            {{- $_ := set $requiredNginxPodAffinity "isRequired" true }}
          {{- end }}
        {{- end }}
      {{- end }}
    {{- end }}
  {{- end }}
{{- end }}
podAffinity:
  requiredDuringSchedulingIgnoredDuringExecution:
    {{- if $requiredPodAffinity.isRequired }}
    - labelSelector:
        matchLabels:
          io.kompose.service: {{ .Values.baseName }}
          {{- include "django.selectorLabels" . | nindent 10 }}
      topologyKey: kubernetes.io/hostname
    {{- end }}
  preferredDuringSchedulingIgnoredDuringExecution:
    {{- if $requiredNginxPodAffinity.isRequired }}
    - podAffinityTerm:
        labelSelector:
          matchLabels:
            io.kompose.service: {{ .Values.nginx.baseName }}
            {{- $nginxConfig := dict "Values" .Values.nginx "Release" .Release "Chart" (dict "Name" (.Values.nginx.nameOverride | default "nginx")) }}
            {{- include "nginx.selectorLabels" $nginxConfig | nindent 12 }}
        topologyKey: kubernetes.io/hostname
      weight: 100
    {{- end }}
podAntiAffinity:
  preferredDuringSchedulingIgnoredDuringExecution:
    {{- if not $requiredPodAffinity.isRequired }}
    # to ensure constant availablility of the application, we deploy different
    # pods on different nodes
    - weight: 100
      podAffinityTerm:
        labelSelector:
          matchLabels:
            io.kompose.service: {{ .Values.baseName }}
            {{- include "django.selectorLabels" . | nindent 12 }}
        topologyKey: kubernetes.io/hostname
    {{- end }}
{{- end }}
