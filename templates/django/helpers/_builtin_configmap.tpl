
{{- define "django.builtinConfig" -}}
ALLOWED_HOSTS: "localhost,django\
  {{- range $hostName := .Values.extraHosts }}
  ,{{ $hostName }}\
  {{- end }}
  {{- range $name, $options := .Values.nginx.routes }}
  {{- if $options.host }}
  ,{{ $options.host }}\
  {{- else }}
  ,{{ $name }}-{{ $.Release.Namespace }}.apps.{{ $.Values.global.openshiftCluster }}.fzg.local\
  {{- end }}
  {{- end }}
  "
DEBUG: ''
DATABASE_ENGINE: postgresql
DATABASE_SERVICE_NAME: postgresql
POSTGRESQL_SERVICE_HOST: postgres
ADMINS: ''
MANAGERS: ''
{{- end }}
