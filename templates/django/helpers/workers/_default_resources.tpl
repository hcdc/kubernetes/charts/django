
{{- define "django.defaultWorkerResources" -}}
limits:
  cpu: "200m"
  memory: "200Mi"
requests:
  cpu: "50m"
  memory: "50Mi"
{{- end }}
