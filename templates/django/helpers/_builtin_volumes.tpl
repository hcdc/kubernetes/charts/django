
{{- define "django.builtinVolumes" -}}
media:
  storage: "1Gi"
  keep: true
  readOnly: false
{{- end }}
