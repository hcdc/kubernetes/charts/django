
Thank you for installing {{ .Chart.Name }}.

Your release is named {{ .Release.Name }}.

{{- if .Release.IsInstall }}
You now need to create secret with the name k8sgitlab containing an SSH-key.

Please create an ssh-keypair with

    ssh-keygen -f id_rsa -q -N ""

Then update the k8sgitlab secret via

    oc set data secret/k8sgitlab --from-file=ssh-privatekey=./id_rsa

And add the contents of the id_rsa.pub file to the Deploy Keys section in

https://{{ .Values.buildSource.gitlabProject | default .Values.global.buildSource.gitlabHost }}/{{ .Values.buildSource.gitlabProject | default .Values.global.buildSource.gitlabProject }}/-/settings/repository#js-deploy-keys-settings

{{- $builtinSecrets := include "django.builtinBuildSecrets" . | fromYaml }}
{{- $allSecrets := mustMergeOverwrite (dict) $builtinSecrets .Values.buildSecrets }}
{{- $secretName := default (printf "build-secret-%s" .Values.baseName) .Values.buildSecretName }}
{{- if (not (empty $allSecrets)) }}
You may now also add secrets via via

{{ range $secretKey, $secretDefault := $allSecrets }}
oc set data secret/{{ $secretName }} --from-file={{ $secretKey }}=./{{ $secretKey }}
{{- end }}
{{- end }}

Afterwards, you can trigger a build and the rollout with
{{- else if .Release.IsUpgrade }}
You can now update your application by running

{{- end }}

    oc start-build {{ .Values.baseName }}
