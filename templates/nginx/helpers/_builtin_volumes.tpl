
{{- define "nginx.builtinVolumes" -}}
media:
  storage: "1Gi"
  keep: true
  mountOnly: true
{{- end }}
