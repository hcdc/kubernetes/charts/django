
{{- define "nginx.builtinBuildTriggers" -}}
triggers:
  - imageChange: {}
    type: ImageChange
  - imageChange:
      from:
        kind: ImageStreamTag
        name: {{ .Values.djangoBaseName }}:latest
    type: ImageChange
{{- end }}
