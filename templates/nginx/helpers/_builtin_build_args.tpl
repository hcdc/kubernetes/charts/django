
{{- define "nginx.builtinBuildArgs" -}}
SERVER_NAME: "127.0.0.1 localhost {{ .Values.baseName }}
  {{- range $name, $options := .Values.routes }}
  {{- if $options.host }}
  {{ $options.host }}
  {{- else }}
  {{ $name }}-{{ $.Release.Namespace }}.apps.{{ $.Values.global.openshiftCluster }}.fzg.local
  {{- end }}
  {{- end }}
  "
djangoimage: {{ printf "%s-quay-registry.apps.%s.fzg.local" (regexFind "dev|prod" .Values.global.openshiftCluster) .Values.global.openshiftCluster }}/{{ required "You need to specify a cluster" .Values.global.openshiftCluster }}_{{ .Release.Namespace }}/{{ .Values.djangoBaseName }}:latest
{{- end }}
