
{{- define "nginx.affinity" -}}
{{- /* due to scoping we need to use a dict for requiredPodAffinity and override the value if necessary */}}
{{- $requiredPodAffinity := dict "isRequired" false }}
{{- $builtinVolumes := include "nginx.builtinVolumes" . | fromYaml }}
{{- $allVolumes := mustMergeOverwrite (dict) $builtinVolumes .Values.volumes }}
{{- range $name, $options := $allVolumes }}
  {{- /* Check the necessity of podAffinity */}}
  {{- if and (not $options.projected) (hasKey $options "accessModes") }}
    {{- range $options.accessModes }}
      {{- if eq . "ReadWriteOnce" }}
        {{- $_ := set $requiredPodAffinity "isRequired" true }}
      {{- end }}
    {{- end }}
  {{- end }}
{{- end }}
podAffinity:
  requiredDuringSchedulingIgnoredDuringExecution:
    {{- if $requiredPodAffinity.isRequired }}
    - labelSelector:
        matchLabels:
          io.kompose.service: {{ .Values.baseName }}
          {{- include "nginx.selectorLabels" . | nindent 10 }}
      topologyKey: kubernetes.io/hostname
    {{- end }}
  preferredDuringSchedulingIgnoredDuringExecution:
    {{- if $requiredPodAffinity.isRequired }}
    - podAffinityTerm:
        labelSelector:
          matchLabels:
            {{- $djangoConfig := dict "Values" (dict "baseName" .Values.djangoBaseName ) "Release" .Release "Chart" (dict "Name" .Values.djangoBaseName ) }}
            {{- include "django.selectorLabels" $djangoConfig | nindent 12 }}
            io.kompose.service: {{ .Values.djangoBaseName }}
        topologyKey: kubernetes.io/hostname
      weight: 100
    {{- end }}
podAntiAffinity:
  preferredDuringSchedulingIgnoredDuringExecution:
    {{- if not $requiredPodAffinity.isRequired }}
    # to ensure constant availablility of the application, we deploy different
    # pods on different nodes
    - weight: 100
      podAffinityTerm:
        labelSelector:
          matchLabels:
            io.kompose.service: {{ .Values.baseName }}
            {{- include "nginx.selectorLabels" . | nindent 12 }}
        topologyKey: kubernetes.io/hostname
    {{- end }}
{{- end }}
